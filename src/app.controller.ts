import {
  Controller,
  Get,
  Param,
  ParseIntPipe,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { AppService } from './app.service';
import { AuthGuard } from './guard/auth.guard';
import { HelloWorldInterceptor } from './interceptor/logging.interceptor';

@Controller('app')
@UseGuards(AuthGuard)
@UseInterceptors(HelloWorldInterceptor)
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getAll() {
    return [];
  }

  @Get(':id')
  getUser(@Param('id', ParseIntPipe) id: number) {
    return this.appService.getUser(id);
  }

  @Get(':id')
  get(@Param('id') id: string) {
    return { id };
  }
}
